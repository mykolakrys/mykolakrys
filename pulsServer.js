const express = require('express');
const fallback = require('express-history-api-fallback');

const app = express();

const root = `${__dirname}/build`;

app.use(express.static(root));
app.use(fallback('index.html', { root }));

const server = app.listen(3030, () => {
  console.log('server is running at %s', server.address().port);
});
