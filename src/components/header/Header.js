import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { crudBuilder } from '../../api';
import dispatchSend from '../../actions/dispatchSend';
import './header.css';

class Header extends Component {
  state = { value: null };

  changeValue = e => this.setState({ value: e.target.value });

  handleClick = async e => {
    e.preventDefault();
    await this.props.dispatchSend(
      'shows',
      crudBuilder('find-film').getList({
        film: this.state.value,
      }),
    );
  };

  render() {
    return (
      <form className="header">
        <input
          type="text"
          placeholder="Input film name"
          onChange={this.changeValue}
        />
        <input
          type="submit"
          value="Search"
          className="button"
          onClick={this.handleClick}
        />
      </form>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ dispatchSend, dispatch }, dispatch);

export default connect(null, mapDispatchToProps)(Header);
