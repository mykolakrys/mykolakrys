import React, { Component } from 'react';
import { connect } from 'react-redux';
import { get } from 'lodash';

import Item from './item';
import './container.css';

class List extends Component {
  get renderList() {
    const { showsData } = this.props;
    if (get(showsData, 'length') > 0) {
      return showsData.map((item, i) => (
        <Item key={i} item={item}/>
      ));
    }
    return <h1 className="loading">No data</h1>;
  }

  render() {
    console.log(this.props);
    const props = this.props;
    return (
      <div className="container">
        {props.showsLoading && <div className="loading">Loading...</div>}
        {!props.showsLoading && this.renderList}
      </div>
    );
  }
}

const mapStateToProps = ({ runtime }) => ({
  showsData: runtime.showsData,
  showsLoading: runtime.showsLoading,
});

export default connect(mapStateToProps)(List);
