import React, { Component } from 'react';
import { get } from 'lodash';
import Popover from 'react-simple-popover';

import './container.css';

class Item extends Component {
  state = { open: false };

  handleClick = e => {
    this.setState({ open: !this.state.open });
  };

  handleClose = e => {
    this.setState({ open: false });
  };

  render() {
    console.log(this.props);
    const props = this.props;
    return (
      <React.Fragment>
        <div className="item" onClick={this.handleClick} ref="target">
          <img src={get(props.item, 'show.image.medium')} />
          <span>{get(props.item, 'show.name')}</span>
        </div>
        <Popover
          show={this.state.open}
          onHide={this.handleClose}
          target={this.refs.target}>
          <p>{get(props.item, 'show.name')}</p>
          <p>{get(props.item, 'show.cast')}</p>
          <p>{get(props.item, 'score')}</p>
          <p>{(get(props.item, 'show.genres') || []).join(', ')}</p>
          <p>{get(props.item, 'show.cash')}</p>
        </Popover>
      </React.Fragment>
    );
  }
}

export default Item;
