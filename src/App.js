import React, { Component } from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import runtime from './reducer/runtime';

import Header from './components/header/Header';
import List from './components/list';

import './index.css';
import './App.css';

const rootReduser = combineReducers({
  runtime,
});

const store = createStore(
  rootReduser,
  composeWithDevTools(applyMiddleware(thunk)),
);
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <React.Fragment>
          <Header />
          <List />
        </React.Fragment>
      </Provider>
    );
  }
}
