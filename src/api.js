import axios from 'axios';

const serverPrefix = 'http://191996-vds-krise96.gmhost.pp.ua:3100/';

const getHeaders = () => ({
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*',
  accept: 'application/json',
});

const sendRequest = (url, options) =>
  axios({
    ...options,
    url: serverPrefix + url,
    headers: getHeaders(),
  });

export const crudBuilder = url => ({
  get: id => sendRequest(`${url}/${id}`, { method: 'GET' }),
  post: data => sendRequest(url, { method: 'POST', data }),
  put: (id, data) => sendRequest(`${url}/${id}`, { method: 'PUT', data }),
  getList: params => sendRequest(url, { method: 'GET', params }),
  deleteRequest: id => sendRequest(`${url}/${id}`, { method: 'DELETE' }),
});
