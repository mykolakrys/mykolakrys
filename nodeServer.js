const express = require('express');
const axios = require('axios');
const cors = require('cors');

const app = express();

// Add headers
app.use(cors());

app.get('/find-film', async (req, res) => {
  const { data } = await axios({
    method: 'GET',
    url: `http://api.tvmaze.com/search/shows?q=${req.query.film}`,
  });
  console.log(data);
  res.send(data);
});

const server = app.listen(3100, () => {
  console.log('server is running at %s', server.address().port);
});
